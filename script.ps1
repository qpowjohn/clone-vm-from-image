#Requires -RunAsAdministrator

$VMNameList = 'k8s-m1','k8s-m2','k8s-m3','k8s-n1','k8s-n2','k8s-n3','k8s-n4'

$RootPath = "C:\Hyper-V\"
$GoldenImagePath = "C:\Hyper-V\ubuntu-image\Virtual Hard Disks\ubuntu-image.vhdx"

$ProcessorPerVM = 2
$MemoryPerVM = 4096

$VMSwitchName = "Local Switch"

# ===== Check =====
if (!(Test-Path $RootPath)){
    Write-Error "RootPath not exists." -ErrorAction Stop
}

if (!(Test-Path $GoldenImagePath)){
    Write-Error "GoldenImagePath not exists." -ErrorAction Stop
}

$ComputerMemory = Get-WmiObject -Class win32_operatingsystem -ErrorAction Stop

if ($MemoryPerVM*$VMNameList.Count -gt $ComputerMemory.FreePhysicalMemory/1024) {
    Write-Error "Memory over usage." -ErrorAction Stop
}

if (!(Get-VMSwitch -Name $VMSwitchName -ErrorAction SilentlyContinue)) {
    Write-Error "VMSwitch not exists." -ErrorAction Stop
}

# ===== Check Finish =====


foreach ($VMName in $VMNameList)
{
    $Path = "$RootPath$VMName"
    
    New-VM -Name $VMName -Generation 2 -MemoryStartupBytes $MemoryPerVM -SwitchName $VMSwitchName -Path $Path -NoVHD
    Copy-Item -Path $GoldenImagePath -Destination $Path

    $VHDName = $GoldenImagePath.Split("\")[-1]
    $VHDXPath = "$Path\$VHDName"

    Add-VMHardDiskDrive -VMName $VMName -Path $VHDXPath
    Set-VM -Name $VMName -ProcessorCount 2
    $FirstBootDevice = Get-VMHardDiskDrive -VMName $VMName
    Set-VMFirmware -VMName $VMName -EnableSecureBoot Off -FirstBootDevice $FirstBootDevice
}
